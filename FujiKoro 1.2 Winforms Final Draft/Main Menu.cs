﻿using System;
using System.Windows.Forms;

namespace FujiKoro_1._2_Winforms_Final_Draft
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtDiplay.Text = $"Welkom Bij Fujikoro Build 1.2\n\nDit project is gemigreerd uit vorige versie op 22/01\n\n" +
                $"Doel is verdere afwerking Map + Maptiles en echte implementatie van de regels en het laden van de data uit txt Files voorlopig" +
                $"\nVerder afwerken Knopfuncties Map, Exploren introducen en Combat methode afwerken. Eventueel PVP gedeelte ook." +
                $"\nAfgewerkte build zou eerste presentatie versie moeten worden." +
                $"\n\n Changelog, Testmap en Exit voorlopig beschikbaar.";
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            Frm_Changelog thischangelog = new Frm_Changelog();
            this.AddOwnedForm(thischangelog);
            thischangelog.ShowDialog();
            if (thischangelog.DialogResult == DialogResult.OK)
            {
                thischangelog.Close();
            }

        }

        private void btnTestMap_Click(object sender, EventArgs e)
        {
            Frm_TestMap thistestmap = new Frm_TestMap();
            this.AddOwnedForm(thistestmap);
            thistestmap.ShowDialog();
            if (thistestmap.DialogResult == DialogResult.OK)
            {
                thistestmap.Close();
            }

        }
    }
}
