﻿using System;
using System.Windows.Forms;

namespace FujiKoro_1._2_Winforms_Final_Draft
{
    public partial class Frm_Changelog : Form
    {
        public Frm_Changelog()
        {
            InitializeComponent();
        }

        private void Frm_Changelog_Load(object sender, EventArgs e)
        {
            txtOutput.Text = $"1e Build started, migration draft 1.0. + Optimalising Names + Fatal Debugging\n" +
                $"\nLog 15/01 :\nAdded Multiresources + Added Testmapspaces + Added Pack + Limits\nAdded Gather + Added Combat + Slay btn + Auto dragonegg + Added multiple status window" +
                $"\nIntegrated and Autoupdated Player/Map objects\n" +
                $"\nLog 16/01: \nAdded different layout items + Added basis for weapondesignmenu + Added remove from pack,\nDebugged previously implemented feats + Added Monk System + Implementation and cmbBoxMonks + MonkfieldsUI\n" +
                $"\nLog 17/01: \nUpdated Combat method so Egg drops on map(not pack), gather function added + checks.\nAdded Weaponcrafting function add resource from pack." +
                $"\n\nLog 18/0: \nMajor Overhaul and inmprovement Weapon System (Lists -> Arrays), Added Logbutton, Pic's, Checks for float/handle's etc + menu\nBackpack Laods correct in Designer + Added Mapdesigner FrontEnd" +
                $"\n\n\n22/01: MIGRATING TO DRAFT 1.2, MAJOR DEBUGGING AND MASSIVE IMPLEMENTATION OF NEW FUNCTIONALITY " +
                $"\n\n\nTO DO:\nMajor : Start Combat Player/Dragon funcions (with buttons/pics for weap select!) + DEBUGGIN SESSION!\n!!!MONKS DONT GO IN BACKPACK! Debug Monk Methods + Implement Monk bonusses to player.\nCreate Premade Weapon for Combat tests. Add Helmet and Sandal Functionality" +
                $"\n" +
                $"\nAFTER THAT : Implement button System for Crafting/Display Gear and Listview!\nDebug everything.";


        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();

        }
    }
}
