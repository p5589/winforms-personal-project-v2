﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace FujiKoro_1._2_Winforms_Final_Draft
{
    public partial class Frm_TestMap : Form
    {
        Button[,] arrMapButtons = new Button[15, 15];
        Mapspace[,] arrMapspaces = new Mapspace[15, 15];
        Player[] theseplayers = new Player[3];
        Player Player1 = new Player("Model1");
        Player Player2 = new Player("Model2");
        Player Player3 = new Player("Model3");
        Player user = new Player("Model1");
        int roundcounter;
        int playerturncounter;

        public Frm_TestMap()
        {
            InitializeComponent();
            roundcounter = 0;
            playerturncounter = 0;
            theseplayers[0] = Player1;
            theseplayers[1] = Player2;
            theseplayers[2] = Player3;
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 15; j++)
                {

                    Mapspace thismapspace = new Mapspace();
                    arrMapspaces[i, j] = thismapspace;
                }
            }
            Mapspace[] starttile = new Mapspace[7];
            for (int i = 0; i < 7; i++)
            {
                Mapspace mymapspace = new Mapspace();
                starttile[i] = mymapspace;
            }
            int x = 0;
            int y = 0;
            int z = 0;

            int posY = 30;
            for (int i = 0; i < 15; i++)
            {
                int posX = 150;
                z = 0;
                for (int j = 0; j < 15; j++)
                {
                    string buttonName = "button" + (x + 1).ToString();
                    Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                    btn.Name = "btn" + y.ToString() + "," + z.ToString();
                    btn.ForeColor = Color.Green;
                    btn.Text = $"{y},{z}";
                    PointF[] points = Polygon(new PointF(30, 30), 6, 30, 0);
                    GraphicsPath polygon_path = new GraphicsPath();
                    polygon_path.AddPolygon(points);
                    Region polygon_region = new Region(polygon_path);
                    btn.Region = polygon_region;


                    if ((i == 0) && (j == 0))
                    {
                        btn.Location = new Point((posX), (posY));
                    }
                    else if ((i % 2) == 0)
                    {
                        btn.Location = new Point(posX, posY);
                    }
                    else
                    {
                        btn.Location = new Point(posX + 30, posY);

                    }
                    this.Controls.Add(btn);
                    btn.MouseHover += (sender, args) =>
                    {
                        string x = btn.Name;
                        x = x.Remove(0, 3);
                        string[] waarden = (x.Split(','));
                        int a = Convert.ToInt32(waarden[0]);
                        int b = Convert.ToInt32(waarden[1]);
                        txtDebug.Text = arrMapspaces[a, b].MapspaceStatus(arrMapspaces[a, b]);

                    };
                    btn.Click += (sender, args) =>
                    {
                        if (btn.Text == "Move here")
                        {
                            string x = btn.Name;
                            x = x.Remove(0, 3);
                            string[] waarden = (x.Split(','));
                            int newposX = Convert.ToInt32(waarden[0]);
                            int newposY = Convert.ToInt32(waarden[1]);
                            int oldposX = user.PlayerpositionXaxis;
                            int oldposY = user.PlayerpositionYaxis;
                            arrMapspaces[oldposX, oldposY].ContainsPlayer = false;
                            arrMapspaces[oldposX, oldposY].MapspacePlayer = null;
                            string buttonName = "btn" + (oldposX).ToString() + "," + (oldposY).ToString();
                            Button oldposition = this.Controls.Find(buttonName, true)[0] as Button;
                            oldposition.Text = $"{oldposX},{oldposY}";

                            for (oldposX = (oldposX - 1); oldposX < (user.PlayerpositionXaxis + 2); oldposX++)
                            {
                                for (oldposY = (oldposY - 1); oldposY < (user.PlayerpositionYaxis + 2); oldposY++)
                                {
                                    buttonName = "btn" + (oldposX).ToString() + "," + (oldposY).ToString();
                                    Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                                    if ((btn.Image != null) && (btn.Name != ("btn" + (user.PlayerpositionXaxis).ToString() + "," + (user.PlayerpositionYaxis).ToString())))
                                    {
                                        if (!arrMapspaces[oldposX, oldposY].Lavaspace)
                                        {
                                            btn.FlatAppearance.BorderColor = Color.Red;
                                            btn.FlatAppearance.BorderSize = 1;
                                            btn.Text = $"{oldposX},{oldposY}";
                                            arrMapButtons[oldposX, oldposY] = btn;
                                        }
                                    }
                                }
                                oldposY = user.PlayerpositionYaxis;

                            }
                            user.PlayerpositionXaxis = newposX;
                            user.PlayerpositionYaxis = newposY;
                            arrMapspaces[newposX, newposY].ContainsPlayer = true;
                            arrMapspaces[newposX, newposY].MapspacePlayer = user;
                            user.CurrentMovepoints--;
                            btn.Text = "P";
                        }


                        //explore functie als enabled en juist!!
                    };
                    if (((i == 6) && (j == 7)) || ((i == 6) && (j == 8)) || ((i == 7) && (j == 6)) || ((i == 7) && (j == 8)) || ((i == 8) && (j == 7)) || ((i == 8) && (j == 8)))
                    {
                        btn.Image = Image.FromFile("Empty.jpg");
                    }
                    else if ((i == 7) && (j == 7))
                    {
                        btn.Image = Image.FromFile("Touw.jpg");
                        btn.Text = "P1";
                        btn.ForeColor = Color.Black;
                        Mapspace thismapspace = arrMapspaces[7, 7];
                        user = theseplayers[playerturncounter];
                        thismapspace.PossibleExplore = false;
                        thismapspace.ContainsPlayer = true;
                        thismapspace.MapspacePlayer = user;

                        arrMapspaces[7, 7] = thismapspace;
                    }
                    arrMapButtons[i, j] = btn;

                    x++;
                    z++;
                    posX = posX + 60;
                }
                y++;
                posY = posY + 45;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();

        }

        private void Frm_TestMap_Load(object sender, EventArgs e)
        {
            txtPlayerDisplay.Text = user.DisplayMapWindow(user);
        }




        private void btnMove_Click(object sender, EventArgs e)
        {
            user.CurrentActiondiscs--;
            user.CurrentMovepoints = (user.CurrentMovepoints + 2);

        }

        private void btnMovePlayer_Click(object sender, EventArgs e)
        {
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;

            for (i = (i - 1); i < (user.PlayerpositionXaxis + 2); i++)
            {
                for (j = (j - 1); j < (user.PlayerpositionYaxis + 2); j++)
                {
                    string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                    Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                    if ((btn.Image != null) && (btn.Name != ("btn" + (user.PlayerpositionXaxis).ToString() + "," + (user.PlayerpositionYaxis).ToString())))
                    {
                        if (!arrMapspaces[i, j].Lavaspace)
                        {


                            btn.Text = "Move here";
                            arrMapButtons[i, j] = btn;
                        }
                    }
                }
                j = user.PlayerpositionYaxis;

            }
        }

        private void btnExpore_Click(object sender, EventArgs e)
        {
            bool answer = false;
            int i = user.PlayerpositionXaxis;
            int j = user.PlayerpositionYaxis;
            if (arrMapspaces[i, j].PossibleExplore)
            {
                if (i % 2 != 0)
                {
                    for (i = (i - 2); i < (user.PlayerpositionXaxis + 3); i++)
                    {
                        for (j = (j - 2); j < (user.PlayerpositionYaxis + 3); j++)
                        {
                            if (((i == user.PlayerpositionXaxis) && (j == user.PlayerpositionYaxis - 2)) || ((i == user.PlayerpositionXaxis) && (j == user.PlayerpositionYaxis + 2)))
                            {
                                string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                                Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                                answer = ChecksurroundingsOddfromOdd(btn);
                                if (!answer)
                                {

                                    if (btn.Image == null)
                                    {
                                        btn.Text = "Explore";
                                        arrMapButtons[i, j] = btn;
                                    }
                                }
                            }
                            if (((i == (user.PlayerpositionXaxis - 2)) && ((j == (user.PlayerpositionYaxis - 1)) || (j == (user.PlayerpositionYaxis + 1)))) || ((i == (user.PlayerpositionXaxis + 2)) && ((j == (user.PlayerpositionYaxis - 1)) || (j == (user.PlayerpositionYaxis + 1)))) || (((i == user.PlayerpositionXaxis - 2) && (j == user.PlayerpositionYaxis)) || ((i == user.PlayerpositionXaxis + 2) && (j == user.PlayerpositionYaxis))))
                            {

                                string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                                Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                                answer = ChecksurroundingsOddfromOdd(btn);
                                if (answer)
                                {

                                    if (btn.Image == null)
                                    {
                                        btn.Text = "Explore";
                                        arrMapButtons[i, j] = btn;
                                    }
                                }
                            }
                            if (((i == (user.PlayerpositionXaxis - 1)) || (i == (user.PlayerpositionXaxis + 1))) && (j == (user.PlayerpositionYaxis - 1)) || ((i == (user.PlayerpositionXaxis - 1)) || (i == (user.PlayerpositionXaxis + 1))) && (j == (user.PlayerpositionYaxis + 1)))
                            {
                                string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                                Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                                answer = ChecksurroundingsEvenfromOdd(btn);
                                if (answer)
                                {

                                    if (btn.Image == null)
                                    {
                                        btn.Text = "Explore";
                                        arrMapButtons[i, j] = btn;
                                    }
                                }
                            }
                        }
                        j = user.PlayerpositionYaxis;
                    }
                }
                else
                {
                    for (i = (i - 2); i < (user.PlayerpositionXaxis + 3); i++)
                    {
                        for (j = (j - 2); j < (user.PlayerpositionYaxis + 3); j++)
                        {
                            if ((i == user.PlayerpositionXaxis) && ((j == user.PlayerpositionYaxis - 2)) || ((i == user.PlayerpositionXaxis) && (j == user.PlayerpositionYaxis + 2)))
                            {
                                string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                                Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                                answer = ChecksurroundingsEvenfromEven(btn);
                                if (!answer)
                                {

                                    if (btn.Image == null)
                                    {
                                        btn.Text = "Explore";
                                        arrMapButtons[i, j] = btn;
                                    }
                                }
                            }
                            if ((j == user.PlayerpositionYaxis) && ((i == (user.PlayerpositionXaxis - 2)) || (i == (user.PlayerpositionXaxis + 2))))
                            {
                                string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                                Button btn = this.Controls.Find(buttonName, true)[0] as Button;
                                answer = ChecksurroundingsEvenfromEven(btn);
                                if (answer)
                                {

                                    if (btn.Image == null)
                                    {
                                        btn.Text = "Explore";
                                        arrMapButtons[i, j] = btn;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        PointF[] Polygon(PointF center, int count, float radius, float angle)
        {
            PointF[] pts = new PointF[6];
            int counter = 0;
            for (float i = angle; i < 360; i += 360f / count)
            {
                float rad = (float)(Math.PI / 180 * i);
                pts[counter] = (new PointF(center.X + (float)Math.Sin(rad) * radius,
                                    center.Y + (float)Math.Cos(rad) * radius));
                counter++;
            }
            return pts;
        }
        private bool ChecksurroundingsOddfromOdd(Button btn)
        {
            bool answer = false;
            int counter = 0;
            string x = btn.Name;
            x = x.Remove(0, 3);
            string[] waarden = (x.Split(','));
            int i = Convert.ToInt32(waarden[0]);
            int j = Convert.ToInt32(waarden[1]);
            for (i = (i - 1); i < (Convert.ToInt32(waarden[0]) + 2); i++)
            {

                for (j = (j - 2); j < (Convert.ToInt32(waarden[1]) + 2); j++)
                {
                    if ((i != Convert.ToInt32(waarden[0])) && (j != Convert.ToInt32(waarden[1])))
                    {
                        string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                        Button btntocheck = this.Controls.Find(buttonName, true)[0] as Button;
                        if (btntocheck.Image == null)
                        {
                            counter++;
                        }
                    }
                }
                j = Convert.ToInt32(waarden[1]);



            }
            if (counter == 6)
            {
                answer = true;
            }
            return answer;
        }
        private bool ChecksurroundingsEvenfromOdd(Button btn)
        {
            bool answer = false;
            int counter = 0;
            string x = btn.Name;
            x = x.Remove(0, 3);
            string[] waarden = (x.Split(','));
            int i = Convert.ToInt32(waarden[0]);
            int j = Convert.ToInt32(waarden[1]);
            for (i = (i - 1); i < (Convert.ToInt32(waarden[0]) + 2); i++)
            {

                for (j = (j - 2); j < (Convert.ToInt32(waarden[1]) + 2); j++)
                {
                    if ((i != Convert.ToInt32(waarden[0])) && (j != Convert.ToInt32(waarden[1])))
                    {
                        string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                        Button btntocheck = this.Controls.Find(buttonName, true)[0] as Button;
                        if (btntocheck.Image == null)
                        {
                            counter++;
                        }
                    }
                }
                j = Convert.ToInt32(waarden[1]);

            }
            j = Convert.ToInt32(waarden[1]) + 1;
            string buttonName2 = "btn" + (i).ToString() + "," + (j).ToString();
            Button btntocheck2 = this.Controls.Find(buttonName2, true)[0] as Button;
            if (btntocheck2.Image == null)
            {
                counter++;
            }
            if (counter == 6)
            {
                answer = true;
            }
            return answer;
        }
        private bool ChecksurroundingsEvenfromEven(Button btn)
        {
            bool answer = false;
            int counter = 0;
            string x = btn.Name;
            x = x.Remove(0, 3);
            string[] waarden = (x.Split(','));
            int i = Convert.ToInt32(waarden[0]);
            int j = Convert.ToInt32(waarden[1]);
            for (i = (i - 1); i < (Convert.ToInt32(waarden[0]) + 2); i++)
            {

                for (j = (j - 2); j < (Convert.ToInt32(waarden[1]) + 2); j++)
                {
                    if ((i != Convert.ToInt32(waarden[0])) && (j != Convert.ToInt32(waarden[1])))
                    {
                        string buttonName = "btn" + (i).ToString() + "," + (j).ToString();
                        Button btntocheck = this.Controls.Find(buttonName, true)[0] as Button;
                        if (btntocheck.Image == null)
                        {
                            counter++;
                        }
                    }
                }
                j = Convert.ToInt32(waarden[1]);



            }
            if (counter == 6)
            {
                answer = true;
            }
            return answer;
        }
    }
}


