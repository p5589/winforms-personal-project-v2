﻿using System.Collections;

namespace FujiKoro_1._2_Winforms_Final_Draft
{
    public class Player
    {

        protected string _playermodel;
        private int _playerpositionXaxis = 7;
        private int _playerpositionYaxis = 7;
        private int _victorypoints = 0;
        private int _healthpoints = 2;
        private int _currentmovepoints = 0;
        private int _maxmovepoints = 2;
        private int _maxactiondiscs = 3;
        private int _currentactiondiscs = 3;
        private int _initiative = 0;
        private Resource[,] _weapon = new Resource[5, 9];
        private Resource[,] _helmet = new Resource[3, 3];
        private Resource[,] _sandals = new Resource[1, 3];
        private ArrayList _backpack = new ArrayList();
        private bool _dualwielding = false;
        private bool _actionmonk = false;
        private bool _sacredmonk = false;
        private bool _weaponmonk = false;
        private bool _backpackmonk = false;
        private Scroll _scrollmove = null;
        private Scroll _scrollgather = null;
        private Scroll _scrollexplore = null;
        private Scroll _scrollinventory1 = null;
        private Scroll _scrollinventory2 = null;

        public Player(string thiscolour)
        {
            _playermodel = thiscolour;
        }
        public string Playermodel
        {
            get { return _playermodel; }
        }
        public int PlayerpositionXaxis
        {
            get { return _playerpositionXaxis; }
            set { _playerpositionXaxis = value; }
        }
        public int PlayerpositionYaxis
        {
            get { return _playerpositionYaxis; }
            set { _playerpositionYaxis = value; }
        }
        public int Victorypoints
        {
            get { return _victorypoints; }
            set { _victorypoints = value; }
        }
        public int Healthpoints
        {
            get { return _healthpoints; }
            set { _healthpoints = value; }
        }
        public int MaxMovepoints
        {
            get { return _maxmovepoints; }
            set { _maxmovepoints = value; }
        }
        public int CurrentMovepoints
        {
            get { return _currentmovepoints; }
            set { _currentmovepoints = value; }
        }
        public int MaxActiondiscs
        {
            get { return _maxactiondiscs; }
            set { _maxactiondiscs = value; }
        }
        public int CurrentActiondiscs
        {
            get { return _currentactiondiscs; }
            set { _currentactiondiscs = value; }
        }

        public Resource[,] Weapon
        {
            get { return _weapon; }
            set { _weapon = value; }
        }
        public int Initiative
        {
            get { return _initiative; }
            set
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        if (_weapon[i, j] != null)
                        {
                            _initiative = j;
                        }
                    }
                }
                if ((Scroll1.ScrollID == 1) || (Scroll2.ScrollID == 1))
                {
                    _initiative++;
                }
            }
        }
        public Resource[,] Helmet
        {
            get { return _helmet; }
            set { _helmet = value; }
        }
        public Resource[,] Sandals
        {
            get { return _sandals; }
            set { _sandals = value; }
        }
        public ArrayList Backpack
        {
            get { return _backpack; }
            set { _backpack = value; }
        }
        public bool Actionmonk
        {
            get { return _actionmonk; }
            set { _actionmonk = value; }
        }
        public bool Sacredmonk
        {
            get { return _sacredmonk; }
            set { _sacredmonk = value; }
        }
        public bool Weaponmonk
        {
            get { return _weaponmonk; }
            set { _weaponmonk = value; }
        }
        public bool Backpackmonk
        {
            get { return _backpackmonk; }
            set { _backpackmonk = value; }
        }
        public bool Dualwielding
        {
            get { return _dualwielding; }
            set { _dualwielding = value; }
        }
        public Scroll Scroll1
        {
            get { return _scrollinventory1; }
            set { _scrollinventory1 = value; }
        }
        public Scroll Scroll2
        {
            get { return _scrollinventory2; }
            set { _scrollinventory2 = value; }
        }


        public string DisplayMapWindow(Player user)
        {
            string output = $"Location ({PlayerpositionXaxis},{PlayerpositionYaxis})\nModel: {Playermodel}\nVictoryPoints:{Victorypoints}\nHealth:{Healthpoints}";
            return output;
        }
    }
}
