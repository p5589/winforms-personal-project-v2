﻿namespace FujiKoro_1._2_Winforms_Final_Draft
{
    public class Scroll
    {
        private string _scrolltxt;
        private int _scrollID;

        public Scroll()
        {

        }

        public Scroll(string scrolltekst, int scrollID)
        {
            _scrolltxt = scrolltekst;
            _scrollID = scrollID;
        }
        public string ScrollText
        {
            get { return _scrolltxt; }
            set { _scrolltxt = value; }
        }
        public int ScrollID
        {
            get { return _scrollID; }
            set { _scrollID = value; }
        }

    }
}
