﻿
namespace FujiKoro_1._2_Winforms_Final_Draft
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChangelog = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnTestMap = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtDiplay = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnChangelog
            // 
            this.btnChangelog.Location = new System.Drawing.Point(65, 369);
            this.btnChangelog.Name = "btnChangelog";
            this.btnChangelog.Size = new System.Drawing.Size(188, 66);
            this.btnChangelog.TabIndex = 0;
            this.btnChangelog.Text = "Changelog";
            this.btnChangelog.UseVisualStyleBackColor = true;
            this.btnChangelog.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(289, 459);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(188, 66);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(65, 459);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(188, 66);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnTestMap
            // 
            this.btnTestMap.Location = new System.Drawing.Point(289, 369);
            this.btnTestMap.Name = "btnTestMap";
            this.btnTestMap.Size = new System.Drawing.Size(188, 66);
            this.btnTestMap.TabIndex = 3;
            this.btnTestMap.Text = "Testmap";
            this.btnTestMap.UseVisualStyleBackColor = true;
            this.btnTestMap.Click += new System.EventHandler(this.btnTestMap_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(506, 369);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(188, 66);
            this.button5.TabIndex = 4;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(506, 459);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(188, 66);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit to Desktop";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtDiplay
            // 
            this.txtDiplay.Location = new System.Drawing.Point(65, 44);
            this.txtDiplay.Name = "txtDiplay";
            this.txtDiplay.Size = new System.Drawing.Size(932, 237);
            this.txtDiplay.TabIndex = 6;
            this.txtDiplay.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 581);
            this.Controls.Add(this.txtDiplay);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnTestMap);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnChangelog);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnChangelog;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnTestMap;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.RichTextBox txtDiplay;
    }
}

