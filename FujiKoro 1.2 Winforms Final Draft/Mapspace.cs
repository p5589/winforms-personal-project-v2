﻿using System.Collections.Generic;

namespace FujiKoro_1._2_Winforms_Final_Draft
{
    class Mapspace
    {
        private bool _containsDragon = false;
        private bool _containsEgg = false;
        private bool _containsResource = false;
        private bool _containsMonk = false;
        private bool _containsPlayer = false;
        private bool _containsScroll = false;
        private bool _lavaspace = false;
        private List<Resource> _mapspaceresources = new List<Resource>();
        private Scroll _mapspacescroll;
        private Dragon _mapspacedragon;
        private Player _mapspaceplayer;
        private Monk _mapspacemonk;
        private DragonEgg _mapspaceegg;
        private int _lavacounter = 0;
        private bool _possibleExplore = true;
        private string _image;

        public Mapspace()
        {
        }

        public Mapspace(bool lavaspace, bool containsDragon, bool containsEgg, bool containsPlayer, bool containsMonk, bool containsResource, bool containsScroll, Player thisPlayer, Dragon thisDragon, List<Resource> theseresources, Scroll thisScroll, Monk thisMonk, DragonEgg thisEgg, int lavacounter, bool possibleexplore, string image)
        {
            ContainsDragon = containsDragon;
            ContainsEgg = containsEgg;
            ContainsPlayer = containsPlayer;
            ContainsMonk = containsMonk;
            ContainsResource = containsResource;
            ContainsScroll = containsScroll;
            MapspacePlayer = thisPlayer;
            MapspaceDragon = thisDragon;
            MapspaceResources = theseresources;
            ThisScroll = thisScroll;
            ThisMonk = thisMonk;
            ThisEgg = thisEgg;
            Lavacounter = lavacounter;
            Lavaspace = lavaspace;
            PossibleExplore = possibleexplore;
            Image = image;


        }
        public bool ContainsDragon
        {
            get { return _containsDragon; }
            set { _containsDragon = value; }
        }
        public bool ContainsEgg
        {
            get { return _containsEgg; }
            set { _containsEgg = value; }
        }
        public bool ContainsPlayer
        {
            get { return _containsPlayer; }
            set { _containsPlayer = value; }
        }
        public bool ContainsMonk
        {
            get { return _containsMonk; }
            set { _containsMonk = value; }
        }
        public bool ContainsResource
        {
            get { return _containsResource; }
            set { _containsResource = value; }
        }
        public bool ContainsScroll
        {
            get { return _containsScroll; }
            set { _containsScroll = value; }
        }
        public bool Lavaspace
        {
            get { return _lavaspace; }
            set { _lavaspace = value; }
        }
        public bool PossibleExplore
        {
            get { return _possibleExplore; }
            set { _possibleExplore = value; }
        }
        public int Lavacounter
        {
            get { return _lavacounter; }
            set { _lavacounter = value; }
        }
        public Player MapspacePlayer
        {
            get { return _mapspaceplayer; }
            set { _mapspaceplayer = value; }
        }
        public Dragon MapspaceDragon
        {
            get { return _mapspacedragon; }
            set { _mapspacedragon = value; }
        }
        public List<Resource> MapspaceResources
        {
            get { return _mapspaceresources; }
            set { _mapspaceresources = value; }
        }
        public Scroll ThisScroll
        {
            get { return _mapspacescroll; }
            set { _mapspacescroll = value; }
        }
        public Monk ThisMonk
        {
            get { return _mapspacemonk; }
            set { _mapspacemonk = value; }
        }
        public DragonEgg ThisEgg
        {
            get { return _mapspaceegg; }
            set { _mapspaceegg = value; }

        }
        public string Image
        {
            get { return _image; }
            set { _image = value; }
        }
        public string MapspaceStatus(Mapspace thismapspace)
        {
            Resource thisresource = new Resource();
            string output = $"Dragon : {thismapspace.ContainsEgg}\nEgg : {thismapspace.ContainsEgg}\nMonk : {thismapspace.ContainsMonk}\nScroll: {thismapspace.ContainsScroll}\nResource: {thismapspace.ContainsResource}" +
                $"\nScrollID: {thismapspace.ThisScroll}\nLava in Round : {thismapspace.Lavacounter}(0 is no Lava)\nExplore from this tile = {thismapspace.PossibleExplore}";
            return output;
        }

    }
}
